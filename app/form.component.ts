import { Component, Inject, AfterViewInit, ChangeDetectorRef,ChangeDetectionStrategy, forwardRef, Provider, ElementRef } from '@angular/core';
import { NgForm, NG_VALUE_ACCESSOR, ControlValueAccessor }    from '@angular/common';

import { Applicant } from './applicant';
import { Calculator } from './calculator';

import { NATIONALITIES, PROPERTYOWNED, PROPERTYTYPE } from './options';

@Component({
    selector: 'myform',
    templateUrl: ['app/form.component.html'],
    changeDetection: ChangeDetectionStrategy.OnPush

})

export class FormComponent {

    calculator = new Calculator();

    nationalities: [];
    propertyowned: [];
    
    public constructor( @Inject(ElementRef) elementRef: ElementRef, public cdr: ChangeDetectorRef) {
        this.nationalities = NATIONALITIES;
        this.propertyowned = PROPERTYOWNED;
        this.propertytype = PROPERTYTYPE;
        this.elementRef = elementRef;

    }

    onChange() {
        this.calculator.calculate();
    }

    triggerSingleApplicant() {
        this.calculator.triggerSingleApplicant();
    }

    triggerJointApplicant() {
        this.calculator.triggerMultiApplicant();
    }

    public setTenure(year) {
        console.log(year);
    }

    initializeSlider() {
        this.sliderEl = $(this.elementRef.nativeElement).find('.slider');
        this.sliderEl.slider({
            orientation: "horizontal",
            min: 0,
            max: this.calculator.maxLoanPeriod,
            step: 1,
            range: false,
            value: this.calculator.loanPeriod,
            slide: (event, ui) => {
                this.calculator.loanPeriod = ui.value;
                this.cdr.markForCheck();
            }
        });
    }

    ngAfterViewInit() {
        this.initializeSlider();
    }

}

export var NATIONALITIES = [
  {value: "S", name: 'Citizen'},
  {value: "PR", name: 'Permanent Resident'},
  {value: "F", name: 'Foreigner'}
];

export var PROPERTYTYPE = [
   {value: "HDB", name: 'HDB'},
   {value: "EC", name: 'EC'},
   {value: "PTE", name: 'Private'},
];

export var PROPERTYOWNED = [
  {value: "0", name: '0'},
  {value: "1", name: '1'},
  {value: ">1", name: '>1'}
];

export var LOANPERIOD = [
  {value: 25, name: '25'},
  {value: 30, name: '30'},
  {value: 35, name: '35'}
]
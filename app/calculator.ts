import { Applicant } from './applicant';

export class Calculator {
    mainApplicant: Applicant = {
        age: 30,
        nationality : "S",
        propertyOwn: "0",
        oustandingLoan: 0,

        fixedIncome: 0,
        variableIncome: 0,
        rentalIncome: 0,
        grossIncome: 0,

        mortgageLoan: 0,
        carLoan: 0,
        cardLoan: 0,
        monthlyExpenses: 0
    };
    jointApplicant: Applicant = {
        age: 30,
        nationality : "S",
        propertyOwn: "0",
        oustandingLoan: 0,

        fixedIncome: 0,
        variableIncome: 0,
        rentalIncome: 0,
        grossIncome: 0,

        mortgageLoan: 0,
        carLoan: 0,
        cardLoan: 0,
        monthlyExpenses: 0
    };

    propertyType = "EC";
    interestRate = 3.5; //annual interest rate
    loanPeriod = 30;  //by year
    totalIncome = 0;
    totalExpenses = 0;
    tdsr60 = 0; //TDSR - 60%
    msr30 = 0;
    tdsr = 0;  //TDSR - 60% less monthly commitment
    cashAvailableMonthly = 0; //cash avalilable for monthly installment
    ltv: number;
    incomeWeightAveAge= 30;
    maxTenure= 35; // loan tenure + age (up to 65 year old)s
    maxLoanPeriod= 35;
    maxLoanAmount = 0;
    maxPropertyPrice = 0;
    
    bsd= 0;
    absdRate= 0;
    absd= 0;
    
    totalCashLayout= 0;
    
    multiApplicants = false;

    public constructor() {

    }

    calculate() {
        this.calGrossIncome();
        this.calMonthlyExpenses();
        this.calTotalIncome();
        this.calTotalExpenses();

        this.calTDSR();
        this.calMSR();

        this.calCashAvailable();
        this.calLTV();
        this.calIncomeWeightAveAge();
        this.calMaxTenure();
        this.calMaxLoanPeriod();
        this.calMaxLoanAmt();
        this.calMaxPropertyPrice();
        this.calBSD();
        this.calABSD();
        this.calCashLayout();
    }
    
    calCashLayout(){
        if (this.maxPropertyPrice > 0){
            this.totalCashLayout = this.maxPropertyPrice - this.maxLoanAmount + this.bsd + this.absd;
        }else{
            this.totalCashLayout = 0;
        }
    }
    
    calABSD(){
        if (this.propertyType == 'PTE'){
            var mainABSD = this.calApplicantPteABSD(this.mainApplicant);
            var jointABSD = this.calApplicantPteABSD(this.jointApplicant);
            this.absdRate = Math.max(mainABSD,jointABSD);
        }else{
            if (this.mainApplicant.nationality == 'S' || this.jointApplicant.nationality == 'S'){
                this.absdRate = 0;
            }else{
                this.absdRate = 0.05;
            }
        }
        this.absd = Math.round(this.absdRate * this.maxPropertyPrice);
    }
    
    calApplicantPteABSD(applicant:Applicant){
        var absd = 0;
        switch (applicant.nationality){
            case 'S':
                if (applicant.propertyOwn == '>1'){
                    absd = 0.1;
                } else if (applicant.propertyOwn == '1'){
                    absd = 0.07;
                } else {
                    absd = 0;
                }
            break;
            case 'PR':
                if (applicant.propertyOwn == '>1'){
                    absd = 0.1;
                } else if (applicant.propertyOwn == '1'){
                    absd = 0.1;
                } else {
                    absd = 0.5;
                }
            break;
            case 'F':
                if (applicant.propertyOwn == '>1'){
                    absd = 0.15;
                } else if (applicant.propertyOwn == '1'){
                    absd = 0.15;
                } else {
                    absd = 0.15;
                }
            break;
        }
        return absd;
    }
    
    calBSD(){
        if (this.maxPropertyPrice > 360000){
            this.bsd = 0.03 * this.maxPropertyPrice - 5400;
        } else if (this.maxPropertyPrice > 180000){
            this.bsd = 0.02 * this.maxPropertyPrice - 1800;
        } else{
            this.bsd = 0.01 * this.maxPropertyPrice;
        }
        this.bsd = Math.round(this.bsd);
    }
    
    calMaxPropertyPrice(){
        this.maxPropertyPrice = Math.round(this.maxLoanAmount / this.ltv);
    }
    
    calMaxLoanAmt(){
        this.maxLoanAmount = Math.round(this.pv(this.interestRate, 12, this.maxLoanPeriod * 12, -this.cashAvailableMonthly, 0));
    }

    calMaxLoanPeriod() {
        this.maxLoanPeriod = Math.min(this.loanPeriod, this.maxTenure);
    }

    calMaxTenure() {
        this.maxTenure = 65 - this.incomeWeightAveAge;
    }

    calIncomeWeightAveAge() {
        this.incomeWeightAveAge = (this.mainApplicant.grossIncome / this.totalIncome) * this.mainApplicant.age + (this.jointApplicant.grossIncome / this.totalIncome) * this.jointApplicant.age;
    }

    calLTV() {
        var totalOustandingLoan = Math.max(this.mainApplicant.oustandingLoan, this.jointApplicant.oustandingLoan);
        if (totalOustandingLoan > 2) {
            totalOustandingLoan = 2;
        }
        switch (totalOustandingLoan) {
            case 0:
                if (this.propertyType == 'HDB') {
                    if (this.maxLoanPeriod <= 25) {
                        this.ltv = 0.8;
                    } else if (this.maxLoanPeriod <= 30) {
                        this.ltv = 0.6;
                    }
                } else if (this.propertyType == 'EC') {
                    if (this.maxLoanPeriod <= 30) {
                        this.ltv = 0.8;
                    } else if (this.maxLoanPeriod <= 35) {
                        this.ltv = 0.6
                    }
                } else if (this.propertyType == 'PTE') {
                    if (this.maxLoanPeriod <= 30) {
                        this.ltv = 0.8;
                    } else if (this.maxLoanPeriod <= 35) {
                        this.ltv = 0.6;
                    }
                }
                break;
            case 1:
                if (this.propertyType == 'HDB') {
                    if (this.maxLoanPeriod <= 25) {
                        this.ltv = 0.5;
                    } else if (this.maxLoanPeriod <= 30) {
                        this.ltv = 0.3;
                    }
                } else if (this.propertyType == 'EC') {
                    if (this.maxLoanPeriod <= 30) {
                        this.ltv = 0.5;
                    } else if (this.maxLoanPeriod <= 35) {
                        this.ltv = 0.3;
                    }
                } else if (this.propertyType == 'PTE') {
                    if (this.maxLoanPeriod <= 30) {
                        this.ltv = 0.5;
                    } else if (this.maxLoanPeriod <= 35) {
                        this.ltv = 0.3;
                    }
                }
                break;
            case 2:
                if (this.propertyType == 'HDB') {
                    if (this.maxLoanPeriod <= 25) {
                        this.ltv = 0.4;
                    } else if (this.maxLoanPeriod <= 30) {
                        this.ltv = 0.2;
                    }
                } else if (this.propertyType == 'EC') {
                    if (this.maxLoanPeriod <= 30) {
                        this.ltv = 0.4;
                    } else if (this.maxLoanPeriod <= 35) {
                        this.ltv = 0.2;
                    }
                } else if (this.propertyType == 'PTE') {
                    if (this.maxLoanPeriod <= 30) {
                        this.ltv = 0.4;
                    } else if (this.maxLoanPeriod <= 35) {
                        this.ltv = 0.2;
                    }
                }
                break;
        }
    }

    calCashAvailable() {
        this.cashAvailableMonthly = Math.min(this.tdsr, this.msr30);
        if (this.cashAvailableMonthly < 0) {
            this.cashAvailableMonthly = 0;
        }
    }

    calMSR() {
        this.msr30 = this.totalIncome * 0.3;
    }

    calTDSR() {
        this.tdsr60 = this.totalIncome * 0.6;
        this.tdsr = this.tdsr60 - this.totalExpenses;
    }

    calTotalExpenses() {
        this.totalExpenses = this.mainApplicant.monthlyExpenses + this.jointApplicant.monthlyExpenses;
    }

    calTotalIncome() {
        this.totalIncome = this.mainApplicant.grossIncome + this.jointApplicant.grossIncome;
    }

    calGrossIncome() {
        this.mainApplicant.grossIncome = this.getGrossIncome(this.mainApplicant);
        this.jointApplicant.grossIncome = this.getGrossIncome(this.jointApplicant);
    }

    calMonthlyExpenses() {
        this.mainApplicant.monthlyExpenses = this.getMonthlyExpenses(this.mainApplicant);
        this.jointApplicant.monthlyExpenses = this.getMonthlyExpenses(this.jointApplicant);
    }

    getGrossIncome(applicant: Applicant) {
        return applicant.fixedIncome + (applicant.variableIncome * 0.7) + (applicant.rentalIncome * 0.7);
    }

    getMonthlyExpenses(applicant: Applicant) {
        return applicant.mortgageLoan + applicant.carLoan + applicant.cardLoan;
    }
    
    pv(rate, per, nper, pmt, fv) {
        nper = parseFloat(nper);
        pmt = parseFloat(pmt);
        fv = parseFloat(fv);
        rate = eval((rate) / (per * 100));
        if ((pmt == 0) || (nper == 0)) {
            //alert("Why do you want to test me with zeros?");
            return (0);
        }
        var pv_value = 0;
        if (rate == 0) // Interest rate is 0
        {
            pv_value = -(fv + (pmt * nper));
        }
        else {
            var x = Math.pow(1 + rate, -nper);
            var y = Math.pow(1 + rate, nper);
            pv_value = -(x * (fv * rate - pmt + y * pmt)) / rate;
        }
        pv_value = this.conv_number(pv_value, 2);
        return (pv_value);
    }

    conv_number(expr, decplaces) { // This function is from David Goodman's Javascript Bible.
        var str = "" + Math.round(eval(expr) * Math.pow(10, decplaces));
        while (str.length <= decplaces) {
            str = "0" + str;
        }
        var decpoint = str.length - decplaces;
        return (str.substring(0, decpoint) + "." + str.substring(decpoint, str.length));
    }
    
    triggerSingleApplicant(){
        this.multiApplicants = false;
    }
    
    triggerMultiApplicant(){
        this.multiApplicants = true;
    }
}

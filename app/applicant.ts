export class Applicant {
  age: number;
  nationality: string;
  propertyOwn: string;
  oustandingLoan: number;
  
  fixedIncome: number;
  variableIncome:number;
  rentalIncome: number;
  grossIncome: number;

  mortgageLoan: number;
  carLoan: number;
  cardLoan: number;
  monthlyExpenses: number;
}



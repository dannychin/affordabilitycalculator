import { Component,ChangeDetectorRef,ChangeDetectionStrategy } from '@angular/core';

import { FormComponent } from './form.component';

@Component({
  selector: 'calculator',
  templateUrl: ['app/app.component.html'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  directives: [
    FormComponent
  ]
})
export class AppComponent { }


/*
Copyright 2016 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/